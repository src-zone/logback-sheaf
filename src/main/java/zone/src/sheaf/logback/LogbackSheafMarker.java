/*
 * Copyright 2014 by The.Src.Zone (Amsterdam, The Netherlands)  
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package zone.src.sheaf.logback;

/**
 * The existence of this class can be used by
 * zone.src.sheaf.properties.ApplicationProperties to run logback specific
 * initialization/configuration code.
 */
public class LogbackSheafMarker {
  // nothing to implement
}